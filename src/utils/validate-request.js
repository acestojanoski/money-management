const fs = require('fs');
const Ajv = require('ajv');
const yaml = require('js-yaml');

const validateRequest = async (context, request, appSettings, businessFunctionName) => {
    const validator = getValidator(context, appSettings, businessFunctionName);

    if (!validator) {
        context.log.error('No validation schema found');
        context.res = {
            status: 404,
            headers: {
                'Content-Type': 'application/json',
            },
            body: {
                error: 'No validation schema found.',
            },
        };
        return context.done();
    }

    const valid = validator(request);

    if (valid) {
        return;
    }

    let err = validator.errors;
    context.res = {
        status: 400,
        headers: {
            'Content-Type': 'application/json',
        },
        body: {
            error: err,
        },
    };

    return context.done();
}

const getValidator = (context, appSettings, businessFunctionName) => {
    try {
        const schemas = yaml.safeLoad(fs.readFileSync('./src/definitions.yaml'), 'utf8');

        const ajv = Ajv({
            allErrors: true,
            schemas: [schemas]
        });

        const validate = ajv.getSchema(appSettings.SCHEMAS_PATH + businessFunctionName);

        if (!validate || typeof validate === 'undefined') {
            return null;
        }

        return validate;

    } catch (err) {
        context.log.error('ERROR: No validation schemas found!\n', err);
        return null;
    }
}

module.exports = validateRequest;
