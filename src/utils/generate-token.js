const jwt = require('jsonwebtoken');

module.exports = (appSettings, user) => {
    const {
        _id,
        firstName,
        lastName,
        email,
    } = user;

    const token = jwt.sign(
        {
            id: _id,
            firstName: firstName,
            lastName: lastName,
            email: email,
        },
        appSettings.TOKEN_SECRET,
        {
            algorithm: appSettings.TOKEN_ALGORITHM,
            expiresIn: appSettings.TOKEN_EXPIRATION,
        }
    );

    return token;
};
