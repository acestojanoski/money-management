const next = {
    response: (context, result, status) => {
        const successResponse = {
            status: status,
            headers: {
                'Content-Type': 'application/json',
            },
            body: result,
        };
    
        context.res = successResponse;
        return context.done();
    },
    error: (context, err) => {
        const errorResponse = {
            status: 500,
            headers: {
                'Content-Type': 'application/json',
            },
            body: err,
        };
    
        context.res = errorResponse;
        return context.done();
    },
};

module.exports = next;
