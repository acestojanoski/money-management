const jwt = require('jsonwebtoken');

const decodeToken = req => {    
    const authorization = req.headers.authorization;
    const token = authorization.split('Bearer ')[1];

    const parsedToken = jwt.decode(token);

    return parsedToken;    
};

module.exports = decodeToken;
