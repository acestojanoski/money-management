const bcrypt = require('bcryptjs');
const saltRounds = 10;

const hashPassword = password => {
    const salt = bcrypt.genSaltSync(saltRounds);
    const hashedPassword = bcrypt.hashSync(password, salt);

    return hashedPassword;
};

module.exports = hashPassword;
