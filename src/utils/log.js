const log = {
    error: (context, err) => {
        context.log.error('ERROR: ', err);
    },
    info: (context, info) => {
        context.log.info('INFO: ', info);
    },
    request: (context, req) => {
        context.log('REQUEST: ', req);
        context.log('REQUEST BODY: ', req.body);
    },
    response: (context, res) => {
        context.log('RESPONSE: ', res);
    },
};

module.exports = log;
