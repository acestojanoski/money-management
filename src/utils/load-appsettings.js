const loadAppSettings = (context) => {
    const appSettings = process.env;
    context.log('APP_SETTINGS:\n', appSettings);
    return appSettings;
}

module.exports = loadAppSettings;
