const jwt = require('jsonwebtoken');

module.exports = (context, req, appSettings) => {
    const authorization = req.headers.authorization;
    if (!authorization) {
        context.res = {
            status: 401,
            headers: {
                'Content-Type': 'application/json',
            },
            body: {
                message: 'Unauthorized',
            },
        };
        return context.done();
    }

    const token = authorization.split('Bearer ')[1];

    jwt.verify(
        token,
        appSettings.TOKEN_SECRET,
        {
            algorithms: appSettings.TOKEN_ALGORITHM,
        },
        (err) => {
            if (err) {
                context.res = {
                    status: 401,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: {
                        message: 'Unauthorized',
                    },
                };
                return context.done();
            }
            return;
        }
    );
};
