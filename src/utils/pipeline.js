const log = require('./log');
const next = require('./next');
const validateRequest = require('./validate-request');
const loadAppSettings = require('./load-appsettings');
const checkAuthorization = require('./check-authorization');

const pipeline = async (context, req, businessFunction, anonymous = false) => {
    log.request(context, req);

    req.body = {
        ...req.body,
        ...req.query,
    };

    const businessFunctionName = businessFunction.name;
    const appsettings = loadAppSettings(context);

    if (!anonymous) {
        await checkAuthorization(context, req, appsettings);
    }

    await validateRequest(context, req.body, appsettings, businessFunctionName);

    await businessFunction(context, req, appsettings, (err, response, status) => {
        if (err) {
            log.error(context, err);
            next.error(context, err);
        } else {
            log.response(context, response);
            if (status) {
                next.response(context, response, status);
            } else {
                next.response(context, response, 200);
            }
        }
    });
};

module.exports = pipeline;
