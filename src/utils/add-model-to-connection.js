const mongoose = require('mongoose');

const addModelToConnection = async (
    appSettings,
    connection,
    schema,
    modelName
) => {
    if (connection == null) {
        connection = await mongoose.createConnection(appSettings.CONNECTION_STRING, {
            bufferCommands: false,
            bufferMaxEntries: 0,
            user: appSettings.DB_USER,
            pass: appSettings.DB_PASSWORD,
        });
        connection.model(modelName, schema);

        return connection;
    }

    connection.model(modelName, schema);
    return connection;
};

module.exports = addModelToConnection;
