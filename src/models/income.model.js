const mongoose = require('mongoose');
const validators = require('./mongoose-validators');

const Schema = mongoose.Schema;
const modelName = 'income';

const userSchema = new Schema({
    category: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'category',
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'user',
    },
    quantity: {
        type: Number,
        required: true,
    },
}, { collection: modelName});

module.exports = {
    name: modelName,
    schema: userSchema,
};
