const mongoose = require('mongoose');
const validators = require('./mongoose-validators');

const Schema = mongoose.Schema;
const modelName = 'expense';

const expenseSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'user',
    },
    category: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'category',
    },
    quantity: {
        type: Number,
        required: true,
    },
}, { collection: modelName});

module.exports = {
    name: modelName,
    schema: expenseSchema,
};
