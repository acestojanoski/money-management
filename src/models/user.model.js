const mongoose = require('mongoose');
const validators = require('./mongoose-validators');

const Schema = mongoose.Schema;
const modelName = 'user';

const userSchema = new Schema({
    firstName: {
        type: String,
        required: true,
        maxlength: 20,
    },
    lastName: {
        type: String,
        required: true,
        maxlength: 20,
    },
    email: {
        type: String,
        unique: true,
        required: true,
        validate: validators.email,
    },
    password: {
        type: String,
        required: true
    }
}, { collection: modelName});

module.exports = {
    name: modelName,
    schema: userSchema,
};
