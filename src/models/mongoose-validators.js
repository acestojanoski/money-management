module.exports = {
    email: {
        validator: function (val) {
            const re = new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/);
            return re.test(val);
        },
        message: props => `${props.value} is not a valid email address!`
    },
};
