const mongoose = require('mongoose');
const validators = require('./mongoose-validators');

const Schema = mongoose.Schema;
const modelName = 'category';

const categorySchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'user',
    },
    name: {
        type: String,
        required: true,
        unique: true,
    }
}, { collection: modelName});

module.exports = {
    name: modelName,
    schema: categorySchema,
};
