const pipeline = require('../utils/pipeline');
const model = require('../models/income.model');
const decodeToken = require('../utils/decode-token');
const createDbConnection = require('../utils/create-db-connection');

let connection = null;

const allIncomes = async (context, req, appSettings, next) => {
    try {
        const parsedToken = decodeToken(req);
        connection = await createDbConnection(appSettings, connection, model.schema, model.name);

        const Income = connection.model(model.name);

        const incomes = await Income.find({
            user: parsedToken.id,
        });
        next(null, incomes, 200);
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, allIncomes);
};
