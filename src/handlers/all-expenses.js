const pipeline = require('../utils/pipeline');
const model = require('../models/expense.model');
const decodeToken = require('../utils/decode-token');
const createDbConnection = require('../utils/create-db-connection');

let connection = null;

const allExpenses = async (context, req, appSettings, next) => {
    try {
        const parsedToken = decodeToken(req);
        connection = await createDbConnection(appSettings, connection, model.schema, model.name);

        const Expense = connection.model(model.name);

        const expenses = await Expense.find({
            user: parsedToken.id,
        });
        next(null, expenses, 200);
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, allExpenses);
};
