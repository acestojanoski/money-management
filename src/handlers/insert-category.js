const pipeline = require('../utils/pipeline');
const model = require('../models/category.model');
const userModel = require('../models/user.model');
const decodeToken = require('../utils/decode-token');
const createDbConnection = require('../utils/create-db-connection');
const addModelToConnection = require('../utils/add-model-to-connection');

let connection = null;

const insertCategory = async (context, req, appSettings, next) => {
    try {
        let request = req.body;
        const parsedToken = decodeToken(req);

        connection = await createDbConnection(appSettings, connection, model.schema, model.name);
        connection = await addModelToConnection(appSettings, connection, userModel.schema, userModel.name);

        const User = connection.model(userModel.name);

        const user = await User.findOne({
            _id: parsedToken.id
        });

        if (!user) {
            next(null, {
                error: 'User not found!',
            }, 404);
        } else {
            const Category = connection.model(model.name);

            const category = new Category({
                ...request,
                user: parsedToken.id,
            });

            await category.save()
                .then(result => {
                    next(null, result, 201);
                })
                .catch(err => {
                    next(err);
                });
        }
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, insertCategory);
};
