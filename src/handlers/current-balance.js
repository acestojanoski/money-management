const pipeline = require('../utils/pipeline');
const expenseModel = require('../models/expense.model');
const incomeModel = require('../models/income.model');
const createDbConnection = require('../utils/create-db-connection');
const addModelToConnection = require('../utils/add-model-to-connection');
const decodeToken = require('../utils/decode-token');

let connection = null;

const currentBalance = async (context, req, appSettings, next) => {
    try {
        let incomesQuantity = 0;
        let expensesQuantity = 0;

        const parsedToken = decodeToken(req);

        connection = await createDbConnection(appSettings, connection, incomeModel.schema, incomeModel.name);
        connection = await addModelToConnection(appSettings, connection, expenseModel.schema, expenseModel.name);

        const Income = connection.model(incomeModel.name);
        const Expense = connection.model(expenseModel.name);

        const incomes = await Income.find({
            user: parsedToken.id,
        });
        const expenses = await Expense.find({
            user: parsedToken.id,
        });

        incomes.forEach(income => {
            incomesQuantity += parseInt(income.quantity, 10);
        });

        expenses.forEach(expense => {
            expensesQuantity += parseInt(expense.quantity, 10);
        });

        const currentBalance = parseInt(incomesQuantity, 10) - parseInt(expensesQuantity, 10);

        next(null, {
            currentBalance,
        }, 200);
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, currentBalance);
};
