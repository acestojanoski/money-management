const pipeline = require('../utils/pipeline');
const model = require('../models/category.model');
const decodeToken = require('../utils/decode-token');
const createDbConnection = require('../utils/create-db-connection');

let connection = null;

const allCategories = async (context, req, appSettings, next) => {
    try {
        const parsedToken = decodeToken(req);
        connection = await createDbConnection(appSettings, connection, model.schema, model.name);

        const Category = connection.model(model.name);

        const categories = await Category.find({
            user: parsedToken.id,
        });
        next(null, categories, 200);
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, allCategories);
};
