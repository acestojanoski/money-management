const pipeline = require('../utils/pipeline');
const model = require('../models/category.model');
const decodeToken = require('../utils/decode-token');
const createDbConnection = require('../utils/create-db-connection');

let connection = null;

const updateCategory = async (context, req, appSettings, next) => {
    try {        
        let request = req.body;
        const parsedToken = decodeToken(req);
        connection = await createDbConnection(appSettings, connection, model.schema, model.name);

        const Category = connection.model(model.name);

        const category = await Category.findOne({
            user: parsedToken.id,
            _id: request.id,
        });

        if (!category) {
            next(null, {
                message: 'Category not found!',
            }, 404);
        } else {
            await category.updateOne(request)
                .then(async result => {
                    next(null, result, 200);
                })
                .catch(err => {
                    next(err);
                });
        }
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, updateCategory);
};
