const pipeline = require('../utils/pipeline');
const model = require('../models/expense.model');
const decodeToken = require('../utils/decode-token');
const categoryModel = require('../models/category.model');
const createDbConnection = require('../utils/create-db-connection');
const addModelToConnection = require('../utils/add-model-to-connection');

let connection = null;

const updateExpense = async (context, req, appSettings, next) => {
    try {
        let request = req.body;
        const parsedToken = decodeToken(req);

        connection = await createDbConnection(appSettings, connection, model.schema, model.name);
        connection = await addModelToConnection(appSettings, connection, categoryModel.schema, categoryModel.name);

        const Expense = connection.model(model.name);
        const Category = connection.model(categoryModel.name);

        const expense = await Expense.findOne({
            user: parsedToken.id,
            _id: request.id,
        });

        if (!expense) {
            next(null, {
                message: 'Expense not found!',
            }, 404);
        } else {
            if (request.category) {
                const category = await Category.findById(request.category);

                if (!category) {
                    next(null, {
                        error: 'Category not found!',
                    }, 404);
                } else {
                    await expense.updateOne(request)
                        .then(async result => {
                            next(null, result, 200);
                        })
                        .catch(err => {
                            next(err);
                        });
                }
            } else {
                await expense.updateOne(request)
                    .then(async result => {
                        next(null, result, 200);
                    })
                    .catch(err => {
                        next(err);
                    });
            }
        }
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, updateExpense);
};
