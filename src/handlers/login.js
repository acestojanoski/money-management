const bcrypt = require('bcryptjs');
const pipeline = require('../utils/pipeline');
const model = require('../models/user.model');
const generateToken = require('../utils/generate-token');
const createDbConnection = require('../utils/create-db-connection');

let connection = null;

const login = async (context, req, appSettings, next) => {
    try {
        let request = req.body;
        request.email = request.email.toLowerCase();

        connection = await createDbConnection(appSettings, connection, model.schema, model.name);

        const User = connection.model(model.name);
        const user = await User.findOne({
            email: request.email
        });

        if (!user) {
            next(null, {
                message: 'User not found!',
            }, 404);
        } else {
            const isAuthenticated = bcrypt.compareSync(
                request.password,
                user.password
            );

            if (!isAuthenticated) {
                const notAuthenticated = {
                    message: 'Wrong password',
                };
                
                next(null, notAuthenticated, 401);
            } else {
                const token = generateToken(appSettings, user);
                next(null, {token});
            }
        }
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, login, true);
};
