const pipeline = require('../utils/pipeline');
const model = require('../models/user.model');
const hashPassword = require('../utils/hash-password');
const createDbConnection = require('../utils/create-db-connection');

let connection = null;

const register = async (context, req, appSettings, next) => {
    try {
        let request = req.body;

        connection = await createDbConnection(appSettings, connection, model.schema, model.name);

        const User = connection.model(model.name);

        const hashedPassword = hashPassword(request.password);
        request.password = hashedPassword;
        request.email = request.email.toLowerCase();

        const user = new User(request);
        await user.save()
            .then(result => {
                next(null, result, 201);
            })
            .catch(err => {
                next(err);
            });
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, register, true);
};
