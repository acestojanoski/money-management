const pipeline = require('../utils/pipeline');
const model = require('../models/income.model');
const userModel = require('../models/user.model');
const decodeToken = require('../utils/decode-token');
const categoryModel = require('../models/category.model');
const createDbConnection = require('../utils/create-db-connection');
const addModelToConnection = require('../utils/add-model-to-connection');

let connection = null;

const insertIncome = async (context, req, appSettings, next) => {
    try {
        let request = req.body;
        const parsedToken = decodeToken(req);

        connection = await createDbConnection(appSettings, connection, model.schema, model.name);
        connection = await addModelToConnection(appSettings, connection, userModel.schema, userModel.name);
        connection = await addModelToConnection(appSettings, connection, categoryModel.schema, categoryModel.name);

        const User = connection.model(userModel.name);
        const Category = connection.model(categoryModel.name);

        const user = await User.findOne({
            _id: parsedToken.id,
        });

        if (!user) {
            next(null, {
                error: 'User not found!',
            }, 404);
        } else {
            const category = await Category.findById(request.category);
            
            if (!category) {
                next(null, {
                    error: 'Category not found!',
                }, 404);
            } else {
                const Income = connection.model(model.name);

                const income = new Income({
                    ...request,
                    user: parsedToken.id,
                });

                await income.save()
                    .then(result => {
                        next(null, result, 201);
                    })
                    .catch(err => {
                        next(err);
                    });
            }
        }
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, insertIncome);
};
