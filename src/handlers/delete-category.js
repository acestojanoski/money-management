const pipeline = require('../utils/pipeline');
const model = require('../models/category.model');
const decodeToken = require('../utils/decode-token');
const incomeModel = require('../models/income.model');
const expenseModel = require('../models/expense.model');
const createDbConnection = require('../utils/create-db-connection');
const addModelToConnection = require('../utils/add-model-to-connection');

let connection = null;

const deleteCategory = async (context, req, appSettings, next) => {
    try {
        let request = req.body;
        const parsedToken = decodeToken(req);
        connection = await createDbConnection(appSettings, connection, model.schema, model.name);
        connection = await addModelToConnection(appSettings, connection, incomeModel.schema, incomeModel.name);
        connection = await addModelToConnection(appSettings, connection, expenseModel.schema, expenseModel.name);

        const Category = connection.model(model.name);
        const Income = connection.model(incomeModel.name);
        const Expense = connection.model(expenseModel.name);

        const incomes = await Income.find({
            user: parsedToken.id,
            category: request.id,
        });

        const expenses = await Expense.find({
            user: parsedToken.id,
            category: request.id,
        });
        
        const category = await Category.findOne({
            user: parsedToken.id,
            _id: request.id,
        });

        if (!category) {
            next(null, {
                message: 'Category not found!',
            }, 404);
        } else {
            if (incomes.length !== 0 || expenses.length !== 0) {
                next(null, {
                    message: 'This category have incomes or expanses. Please delete them before you delete this category.',
                }, 403);
            } else {
                await category.remove()
                    .then(result => {
                        next(null, result, 202);
                    })
                    .catch(err => {
                        next(err);
                    });
            }
        }
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, deleteCategory);
};
