const pipeline = require('../utils/pipeline');
const model = require('../models/income.model');
const decodeToken = require('../utils/decode-token');
const createDbConnection = require('../utils/create-db-connection');

let connection = null;

const deleteIncome = async (context, req, appSettings, next) => {
    try {        
        let request = req.body;
        const parsedToken = decodeToken(req);
        connection = await createDbConnection(appSettings, connection, model.schema, model.name);

        const Income = connection.model(model.name);

        const income = await Income.findOne({
            user: parsedToken.id,
            _id: request.id,
        });

        if (!income) {
            next(null, {
                message: 'Income not found!',
            }, 404);
        } else {
            await income.remove()
                .then(result => {
                    next(null, result, 202);
                })
                .catch(err => {
                    next(err);
                });
        }
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, deleteIncome);
};
