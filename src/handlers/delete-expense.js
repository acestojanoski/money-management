const pipeline = require('../utils/pipeline');
const model = require('../models/expense.model');
const decodeToken = require('../utils/decode-token');
const createDbConnection = require('../utils/create-db-connection');

let connection = null;

const deleteExpense = async (context, req, appSettings, next) => {
    try {        
        let request = req.body;
        const parsedToken = decodeToken(req);
        connection = await createDbConnection(appSettings, connection, model.schema, model.name);

        const Expense = connection.model(model.name);

        const expense = await Expense.findOne({
            user: parsedToken.id,
            _id: request.id,
        });

        if (!expense) {
            next(null, {
                message: 'Expense not found!',
            }, 404);
        } else {
            await expense.remove()
                .then(result => {
                    next(null, result, 202);
                })
                .catch(err => {
                    next(err);
                });
        }
    } catch (err) {
        next(err);
    }
};

module.exports = async (context, req) => {
    await pipeline(context, req, deleteExpense);
};
