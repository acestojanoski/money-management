# money-management

## Description

Simple money management API created using Azure Functions with Node.js

## Technologies and libraries

*   Node.js
    - [ajv](https://github.com/epoberezkin/ajv)
    - [bcryptjs](https://github.com/dcodeIO/bcrypt.js)
    - [js-yaml](https://github.com/nodeca/js-yaml)
    - [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken)
    - [mongoose](https://github.com/Automattic/mongoose)
*   MongoDB database
